package aula20190430.introducao_a_sockets;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ClientAEP {
	public static void main(String[] args) {
		ClientAEP c = new ClientAEP();
		c.start();
	}
	
	private void start() {
		try(Socket server = new Socket("localhost",9092)) {
			Scanner console =  new  Scanner(server.getInputStream());
			PrintWriter toServer = new PrintWriter(server.getOutputStream());
			Scanner fromServer =  new  Scanner(server.getInputStream());
			do {
				System.out.println("Envie comando para o server >> ");
				String comando = console.nextLine();
				toServer.println(comando);
				toServer.flush();
				String resposta = fromServer.nextLine();
				System.out.println("Resposta " + resposta);
			} while (true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
		public void sendFile(String filePath, Socket server) throws IOException {
		DataOutputStream dos = new DataOutputStream(server.getOutputStream());
		FileInputStream fis = new FileInputStream(filePath);
		byte[] buffer = new byte[4096];

		while (fis.read(buffer) > 0) {
			dos.write(buffer);
		}

		fis.close();
		dos.close();
	}
	
	
}