package aula20190430.introducao_a_sockets;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerAEP {
	private static final int PORT = 9092;
	
	public static void main(String[] args) {
		Server s = new Server();
		s.start();
	}
	
	public void start() {
		System.out.println("Inicializando o servidor...");
		try (ServerSocket socket = new ServerSocket(PORT)){	
			System.out.println("Servidor ouvindo na porta " + PORT);
			Socket toClient = socket.accept();
			
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Servidor finalizado.");
	}

	
	public void lerDiretorio() {
		try {
			File raiz = new File("d:/dados");
			exibirDiretorios(raiz,"");
			File paraRemover = new File("d:/dados/arquivoSocket.txt");
			paraRemover.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private static void exibirDiretorios(File entrada, String identacao) {
		System.out.println(identacao + entrada.getAbsolutePath());
		if(entrada.isDirectory()) {
			for(File arquivo : entrada.listFiles()) {
				exibirDiretorios(arquivo, identacao + " ");
			}
		}
	}
	
	
	private void salvarArquivo(Socket clientSock, String path, String fileExtension) throws IOException {
		DataInputStream dis = new DataInputStream(clientSock.getInputStream());
		FileOutputStream fos = new FileOutputStream(path + "/NewFile." + fileExtension);
		byte[] buffer = new byte[4096];

		int filesize = 15123;
		int ler = 0;
		int totalLer = 0;
		int remaining = filesize;
		while ((ler = dis.ler(buffer, 0, Math.min(buffer.length, remaining))) > 0) {
			totalRead += ler;
			remaining -= ler;
			System.out.println("ler " + totalLer + " bytes.");
			fos.write(buffer, 0, ler);
		}

		fos.close();
		dis.close();
	}

}
